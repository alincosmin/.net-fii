using System;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using SimpleChatSystem.Common;

namespace SimpleChatSystem.WPF
{
    public class WPFNotifier : MarshalByRefObject, INotifier
    {
        [STAThread]
        public void NotifyNewMessage(IMessage message)
        {
            var thread = new Thread(() =>
            {
                var dialog = new Reply();
                dialog.Prepare(message);
                dialog.Show();


                System.Windows.Threading.Dispatcher.Run();
            });

            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        public void NotifyUserStatusChange(IUser user)
        {
            if (user.IsOnline(MainWindow.connectedUser))
            {
                MessageBox.Show(string.Format("{0} is now online", user.Name));
            }
            else
            {
                MessageBox.Show(string.Format("{0} is now offline", user.Name));
            }
        }
    }
}