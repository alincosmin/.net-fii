﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SimpleChatSystem.Common;

namespace SimpleChatSystem.WPF
{
    /// <summary>
    /// Interaction logic for Reply.xaml
    /// </summary>
    public partial class Reply : Window
    {
        private IUser _sendTo;

        public Reply()
        {
            InitializeComponent();
        }

        public void Prepare(IMessage message)
        {
            _sendTo = message.Sender;
            fromLabel.Content = _sendTo.Name;
            timeLabel.Content = message.Timestamp.ToString("HH:mm:ss");
            msgTxtBox.Text = message.Content;
        }

        private void replyButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.connectedUser.SendDirectMessage(_sendTo, newMsgTxtBox.Text);
            this.Close();
        }
    }
}
