﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SimpleChatSystem.Common;

namespace SimpleChatSystem.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int _clientPort;
        private int _serverPort;
        public static IChatService _chatService;
        public static IUserService _userService;
        public static IUser connectedUser;

        private IUser selectedUser;

        public MainWindow()
        {
            InitializeComponent();
            _serverPort = 63236;
            var rnd = new Random();
            _clientPort = _serverPort + (rnd.Next() % 100) * (int)(Math.Pow(-1, rnd.Next() % 2));

            InitialConnect();
        }

        private void InitialConnect()
        {
            var serverProvider = new BinaryServerFormatterSinkProvider();
            serverProvider.TypeFilterLevel = TypeFilterLevel.Full;
            var props = new Hashtable();
            props["port"] = _clientPort;

            var channel = new TcpChannel(props, null, serverProvider);
            ChannelServices.RegisterChannel(channel, false);
            _chatService =
                (IChatService)Activator.GetObject(typeof(IChatService), "tcp://localhost:" + _serverPort + "/Chat");
            _userService =
                (IUserService)Activator.GetObject(typeof(IUserService), "tcp://localhost:" + _serverPort + "/Account");

            RemotingConfiguration.RegisterWellKnownServiceType(typeof(WPFNotifier), "ClientNotifier",
                WellKnownObjectMode.Singleton);

        }

        private void FriendListBox_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            selectedUser = _userService.GetUser((string) friendListBox.SelectedItem);
            if (selectedUser != null)
            {
                usernameLabel.Content = string.Format("Send to: {0} - {1}line", selectedUser.Name,
                    selectedUser.IsOnline(connectedUser) ? "on" : "off");
            }
            else
            {
                usernameLabel.Content = "Send to: ";
            }
            messageTxtBox.Text = string.Empty;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            AttemptLogin();

            if (connectedUser != null)
            {
                MessageBox.Show("Successfully connected as " + usernameInput.Text);
                RefreshFriendList();
            }

            usernameInput.IsEnabled = false;
            loginButton.IsEnabled = false;
        }

        private void AttemptLogin()
        {
            try
            {
                connectedUser = _userService.Login(usernameInput.Text, _clientPort);
            }
            catch (SocketException)
            {
                MessageBox.Show("Can't connect!");
                loginButton.IsEnabled = true;
            }
        }

        public void RefreshFriendList()
        {
            var users = _userService.GetOnlineUsers();
            var tranformed = users.Select(x => x.Name);
            friendListBox.ItemsSource = tranformed;
            friendListBox.Items.Refresh();
        }

        private void sendButton_Click(object sender, RoutedEventArgs e)
        {
            connectedUser.SendDirectMessage(selectedUser, messageTxtBox.Text);
            messageTxtBox.Text = string.Empty;
        }
    }
}
