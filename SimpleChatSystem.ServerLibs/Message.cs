﻿using System;
using SimpleChatSystem.Common;

namespace SimpleChatSystem.ServerLibs
{
    public class Message : MarshalByRefObject, IMessage
    {
        private DateTime _initTime;

        public IUser Sender { get; set; }
        public IUser Receiver { get; set; }
        public string Content { get; set; }

        public DateTime Timestamp
        {
            get { return _initTime; } 
            set { }
        }

        public Message(IUser from, IUser to, string message)
        {
            Sender = from;
            Receiver = to;
            _initTime = DateTime.Now;
            Content = message;
        }
    }
}