﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using SimpleChatSystem.Common;

namespace SimpleChatSystem.ServerLibs
{
    public class User : MarshalByRefObject, IUser
    {
        public string Name { get; set; }
        private IEnumerable<IUser> _friends;
        private bool _isOnline;

        public INotifier Notifier { get; set; }


        public User(string name, bool online, INotifier notifier)
        {
            Name = name;
            _friends = new List<IUser>();
            _isOnline = online;
            Notifier = notifier;
        }

        public bool IsOnline(IUser user)
        {
            //return _friends.Contains(user) && _isOnline;
            return _isOnline;
        }

        public IMessage CreateMessageFor(IUser friend, string message)
        {
            return new Message(this, friend, message);
        }

        public void SendDirectMessage(IUser user, string message)
        {
            var msg = new Message(this, user, message);
            user.Notifier.NotifyNewMessage(msg);
        }
    }
}
