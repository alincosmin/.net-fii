﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;
using SimpleChatSystem.Common;

namespace SimpleChatSystem.ServerLibs
{
    public class UserService : MarshalByRefObject, IUserService
    {
        public IList<IUser> OnlineUsers { get; protected set; }

        public UserService()
        {
            OnlineUsers = new List<IUser>();   
        }

        public IUser GetUser(string name)
        {
            Console.WriteLine("Cauta {0}...", name);
            return OnlineUsers.FirstOrDefault(x => x.Name.Equals(name));
        }

        public IUser Login(string username, int port)
        {
            var notifier = (INotifier) Activator.GetObject(typeof (INotifier), "tcp://localhost:" + port +"/ClientNotifier");
            var user = new User(username, true, notifier);

            foreach (var u in OnlineUsers)
            {
                    u.Notifier.NotifyUserStatusChange(user);
            }

            OnlineUsers.Add(user);
            return user;
        }

        public IList<IUser> GetOnlineUsers()
        {
            return OnlineUsers;
        }
    }
}