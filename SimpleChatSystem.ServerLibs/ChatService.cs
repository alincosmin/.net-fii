﻿using System;
using System.Collections;
using System.Collections.Generic;
using SimpleChatSystem.Common;

namespace SimpleChatSystem.ServerLibs
{
    public class ChatService : MarshalByRefObject, IChatService
    {

        public void SendMessage(IMessage message)
        {
            Console.WriteLine("From: {0}", message.Sender.Name);
            Console.WriteLine("To: {0}", message.Receiver.Name);
            Console.WriteLine("Text: {0}\n", message.Content);
            message.Receiver.Notifier.NotifyNewMessage(message);
        }
    }
}