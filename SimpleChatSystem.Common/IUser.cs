﻿using System.Collections.Generic;

namespace SimpleChatSystem.Common
{
    public interface IUser
    {
        string Name { get; set; }
        INotifier Notifier { get; set; }
        bool IsOnline(IUser friend);
        IMessage CreateMessageFor(IUser friend, string message);
        void SendDirectMessage(IUser user, string message);
    }
}