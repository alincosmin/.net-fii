﻿using System;
using System.Collections.Generic;

namespace SimpleChatSystem.Common
{
    public interface IUserService
    {
        IUser GetUser(string user);
        IUser Login(string username, int port);
        IList<IUser> GetOnlineUsers();
    }
}