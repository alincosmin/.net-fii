﻿using System;

namespace SimpleChatSystem.Common
{
    public interface IChatService
    {
        void SendMessage(IMessage message);
    }
}