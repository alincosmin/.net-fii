﻿namespace SimpleChatSystem.Common
{
    public interface INotifier
    {
        void NotifyNewMessage(IMessage message);
        void NotifyUserStatusChange(IUser user);
    }
}