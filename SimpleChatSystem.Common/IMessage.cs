﻿using System;

namespace SimpleChatSystem.Common
{
    public interface IMessage
    {   
        IUser Sender { get; set; }
        IUser Receiver { get; set; }
        DateTime Timestamp { get; set; }
        string Content { get; set; }
    }
}
