﻿using System;
using System.Collections;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;
using System.Threading;
using SimpleChatSystem.Common;

namespace SimpleChatSystem.ConsoleClient
{
    class Client
    {
        private static IUser selfUser;
        public static void Main(string[] args)
        {
            var username = args.Length < 1 ? "admin" : args[0];
            var username2 = args.Length < 2 ? "cineva" : args[1];

            var serverPort = 63236;
            var rnd = new Random();
            var clientPort = serverPort + (rnd.Next()%100)* (int)(Math.Pow(-1, rnd.Next()%2));

            Console.WriteLine("This is Client");
            var instance = new Client();


            var serverProvider = new BinaryServerFormatterSinkProvider();
            serverProvider.TypeFilterLevel = TypeFilterLevel.Full;
            var props = new Hashtable();
            props["port"] = clientPort;

            var channel = new TcpChannel(props, null, serverProvider);
            ChannelServices.RegisterChannel(channel, false);
            var chatService =
                (IChatService) Activator.GetObject(typeof (IChatService), "tcp://localhost:" + serverPort + "/Chat");
            var userService =
                (IUserService) Activator.GetObject(typeof (IUserService), "tcp://localhost:" + serverPort + "/Account");

            RemotingConfiguration.RegisterWellKnownServiceType(typeof (ClientSideNotifier), "ClientNotifier",
                WellKnownObjectMode.Singleton);


            

            Console.WriteLine("Conectat ca {0} pe portul {1}", selfUser.Name, clientPort);

            IUser friend = null;
            do
            {
                friend = userService.GetUser(username2);
                Thread.Sleep(2000);
            } while (friend == null);

            Console.WriteLine("{0} pare a fi ok", friend.Name);

            selfUser.SendDirectMessage(friend, "baa");

            Console.WriteLine("My name is: {0}", selfUser.Name);
            Console.WriteLine("");
            Console.Read();
        }

    }
}
