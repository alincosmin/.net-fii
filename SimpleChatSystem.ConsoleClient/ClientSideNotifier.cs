﻿using System;
using SimpleChatSystem.Common;

namespace SimpleChatSystem.ConsoleClient
{
    public class ClientSideNotifier : MarshalByRefObject, INotifier
    {
        public void NotifyNewMessage(IMessage message)
        {
            Console.WriteLine("Ai primit mesaj de la {0}: {1}", message.Sender.Name, message.Content);
        }

        public void NotifyUserStatusChange(IUser user)
        {
            throw new NotImplementedException();
        }
    }
}