﻿using System;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;
using SimpleChatSystem.Common;
using SimpleChatSystem.ServerLibs;

namespace SimpleChatSystem.ConsoleServer
{
    class Server
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This is Server");

            var serverProvider = new BinaryServerFormatterSinkProvider();
            serverProvider.TypeFilterLevel = TypeFilterLevel.Full;
            var props = new Hashtable();
            props["port"] = 63236;

            var channel = new TcpChannel(props, null, serverProvider);

            ChannelServices.RegisterChannel(channel, false);
            RemotingConfiguration.RegisterWellKnownServiceType(typeof(ChatService), "Chat", WellKnownObjectMode.Singleton);
            RemotingConfiguration.RegisterWellKnownServiceType(typeof(UserService), "Account", WellKnownObjectMode.Singleton);

            Console.Read();
        }
    }
}
